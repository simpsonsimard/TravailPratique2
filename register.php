
    <?php
    /*
    * Titre : register.php
    * Description : formulaire d'inscription au site
    * voir : /control/utilisateur/edit.php
    */
    include_once('include/header.php'); ?>
    <div class="container">
          <?php
          if (isset($_GET['code']))
          {
            if ($_GET['code']==1) {
              ?>
              <div class="alert error">
                <p>
              <?php
              echo "Certains champs n'ont pas été remplis.";
            }
            if ($_GET['code']==2) {
              ?>
              <div class="alert error">
                <p>
              <?php
              echo "Le nom d'utilisateur est déjà utilisé. ";
            }
            if ($_GET['code']==3) {
              ?>
              <div class="alert error">
                <p>
              <?php
              echo "Vous n'avez pas rempli le formulaire correctement.";
            }
            if ($_GET['code']==0) {
              ?>
              <div class="alert succes">
                <p>
              <?php
              echo "Votre compte a été créé avec succès.";
            }?>
          </p>
          </div>
          <?php }?>
      <form class="formulaireConnection row" action="control/utilisateur/register.php" method="POST">
        <label for="name">Nom :</label>
        <input type="text" id="" value="" name="name"><br>
        <label for="prenom">Prenom :</label>
        <input type="text" id="" value="" name="prenom"><br>
        <label for="user">Nom utilisateur :</label>
        <input type="text" id="" value="" name="user"><br>
        <label for="pass">Mot de passe :</label>
        <input type="password" id="" value="" name="pass"><br>
        <label for="email">Courriel :</label>
        <input type="text" id="" value="" name="email"><br>
        <label for="adresse">Adresse :</label>
        <input type="text" id="" value="" name="adresse"><br>
        <label for="ville">Ville :</label>
        <input type="text" id="" value="" name="ville"><br>
        <label for="province">Province :</label>
        <input type="text" id="" value="Quebec" name="province"><br>
        <label for="codePostal">Code Postal :</label>
        <input type="text" id="" value="" name="codePostal"><br>
        <input type="submit" id="" value="Enregistrer">
      </form>
    </div>

    <?php require_once('include/footer.php'); ?>
</body>
</html>
