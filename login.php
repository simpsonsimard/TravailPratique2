
<?php
/*
* Titre : login.php
* Description : Formulaire de connexion au site
*/
include_once('include/header.php');
?>
<div class="container">
  <?php if (isset($_GET['code']))
  {
    ?>
    <div class="alert error">
      <p>
    <?php
    if ($_GET['code']==1) {
      echo 'Vous devez entrer les informations.';
    }
    if ($_GET['code']==2) {
      echo 'Les informations sont incorrectes.';
    }
    ?>
  </p>
</div>
    <?php
  } ?>
  <div class='formulaireConnection'>

    <form action="control/utilisateur/login.php" method="POST">
      <input type="text" id="" value="" name="user" placeholder="Identifiant">
      <input type="password" id="" value="" name="password"placeholder="Mot de passe">
      <input type="submit" id="" value="Connexion">
    </form>
  </div>

</div>
<?php require_once('include/footer.php'); ?>
</body>
</html>
