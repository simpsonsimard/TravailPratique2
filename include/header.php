<?php
/*
* Titre : header.php
* Description : Fichier de configuration du header du site internet
*/
session_start(); include_once('include/head.php'); ?>
<body>
  <header>
    <div class="container">
      <div class="header1">
        <ul class="socialcontainer">
          <li class="left"><a href="panier.php"><i class="fa fa-shopping-bag"><?php
          if (isset($_SESSION["panier"])) {
            echo array_sum($_SESSION["panier"]['qte']);
          }else {
            echo '0';
          }?></a></i></li>
          <?php
          if(isset($_SESSION['utilisateur'])){
            echo "<li><a href='profil.php'>".$_SESSION['utilisateur']['prenom']."</a></li>";
            echo "<li><a href='control/utilisateur/logout.php'>Déconnexion</a></li>";
          }else{
            ?>
            <li><a href="login.php">Connexion</a></li>
            <li><a href="register.php">Inscription</a></li>
            <?php
          }
          ?>
        </ul>
      </div><!--/header1 clearfix -->
      <div class="header2">
        <div class="logo">
          <a href="index.php"><img src="assets/img/index/Sportif.png"></img></a>
        </div>
        <nav>
          <ul>
            <a href="index.php"><li>Accueil</li></a>
            <a href="produits.php"><li>Produits</li></a>
            <a href="profil.php"><li>Profil</li></a>
            <a href="panier.php"><li>Panier</li></a>
          </ul>
        </nav>
      </div><!--/header2 -->
    </div>
  </header>
  <?php
