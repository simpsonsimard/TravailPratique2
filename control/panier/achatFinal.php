<?php
/*
* Titre : achatFinal.php
* Description : Ce fichier ajoute le panier à la commande dans la base de données et efface le panier
* voir /control/panier/afficherpanier.php pour le post
*/
//on se connecte a la base de données
require('../param_bd.inc');
//on recupere la session
session_start();
$current_url = explode('?',  $_SERVER['HTTP_REFERER']);


if (isset($_SESSION["utilisateur"]) AND isset($_SESSION['panier']) AND !empty($_SESSION['panier']['no']) AND isset($_POST['typePaiment']) AND isset($_POST['noCarte'])) {
  //on prepare une requête pour voir où sont rendues les clés automatiques
  $req = $connection->prepare("SHOW TABLE STATUS FROM tp2_evo_ms LIKE 'commandes'");
  $req->execute();
  $command = $req->fetch();
  $req->closeCursor();
  date_default_timezone_set('UTC');
  $req = $connection->prepare('INSERT INTO commandes(date, statut, typePaiement, noCarte, noClient) VALUES (:dates,:statut,:typepaiment,:noCarte,:noClient)');
  $commandecomplet=true;
  $req->execute(array(
    'dates' => date('Y-m-d'),//la date d'aujourd'hui
    'statut' => '1',
    'typepaiment' => $_POST['typePaiment'],
	'noCarte' => $_POST['noCarte'],
    'noClient' => $_SESSION['utilisateur']['no']));
    $req->closeCursor();
    $qteProduit= Array();
    for ($i=0; $i < count($_SESSION['panier']['no']); $i++) {
      $req = $connection->prepare('SELECT qte FROM produits WHERE no = :no');
      $req->execute(array('no'=>$_SESSION['panier']['no'][$i]));

      $qteProduit[$i] = $req->fetch();
      print_r($qteProduit);
      print_r($_SESSION['panier']['qte'][$i]);
        echo "<br>";
      $req->closeCursor();
      if (intval($_SESSION['panier']['qte'][$i])>intval($qteProduit[$i]['qte'])) {
        $commandecomplet=false;
      }
    }
    echo "<br>";
    if ($commandecomplet===true) {
      for ($i=0; $i < count($_SESSION['panier']['no']); $i++) {
        $req = $connection->prepare('INSERT INTO items_commande(noCommande, noProduit, qte) VALUES (:noCommande,:noProduit,:qte)');
        $req->execute(array(
          'noCommande' => $command['Auto_increment'],
          'noProduit' => $_SESSION['panier']['no'][$i],
          'qte' => $_SESSION['panier']['qte'][$i]));
          $req->closeCursor();
        $req = $connection->prepare('UPDATE produits SET qte=:newQte WHERE no=:no');
        $newStock = $qteProduit[$i]['qte']-$_SESSION['panier']['qte'][$i];
        $req->execute(array(':newQte'=>$newStock,'no'=>$_SESSION['panier']['no'][$i]));

      }
    }
      if ($commandecomplet===false) {
      header('Location: ' . $current_url[0].'?code=2');
      exit();
    }else {
      unset($_SESSION['panier']);
    }

      header('Location: ' . $current_url[0].'?code=0');
      exit();
 }else {
      /*Si l'utilisateur n'est pas loggué on le redirige vers le panier avec un message d'erreur*/
      header('Location: ' . $current_url[0].'?code=1');
 }

 ?>
