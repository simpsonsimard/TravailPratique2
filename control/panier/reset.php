<?php
/*
* Titre : reset.php
* Description : Permet de vider tout le panier d'un coup
* note :
*/
session_start();
//on supprime la variable panier
unset($_SESSION['panier']);
//on revient sur la page précédente.
header('Location: ' . $_SERVER['HTTP_REFERER']);
?>
