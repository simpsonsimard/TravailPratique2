<?php
/*
* Titre : afficherpanier.php
* Description : gestion et affichage des données du panier
*/
//connexion BD
require('control/param_bd.inc');
?>
<div class="container">
<?php
if (isset($_GET['code']))
{
  if ($_GET['code']==2) {
    ?>
    <div class="alert error">
      <p>
    <?php
    echo "Certains élements de votre panier ne sont pas en quantité suffisante.";
  }
  if ($_GET['code']==1) {
    ?>
    <div class="alert error">
      <p>
    <?php
    echo 'Vous devez être connecté pour envoyer votre commande.';
  }
  if ($_GET['code']==0) {
    ?>
    <div class="alert succes">
      <p>
    <?php
    echo "Votre commande a été envoyée. Merci d'avoir magasiné chez nous.";
  }
?></p>
</div><?php
}
  ?>

  <table class="tablePanier">
    <tr>
      <td class="panierHeader">Supprimer</td>
      <td class="panierHeader">Produit</td>
      <td class="hidden-xs, panierHeader">Description</td>
      <td class="panierHeader">Quantité</td>
      <td class="panierHeader">Prix unitaire</td>
      <td style="width:10%" class="panierHeader">Prix</td>
    </tr>
    <?php
    //on vérifie que le panier existe et qu'il n'est pas vide
    if (isset($_SESSION['panier']) AND !empty($_SESSION['panier']['no'])) {
      //on boucle pour tous les elements dans le panier
      $total=0;
      for ($i=0; $i < count($_SESSION['panier']['no']); $i++) {
        $req = $connection->prepare('SELECT * FROM produits WHERE no = :arraydonnees');
        $req->execute(array('arraydonnees'=> $_SESSION['panier']['no'][$i]));
        $produit = $req->fetch();
        $total+=$_SESSION['panier']['qte'][$i]*$produit['prix'];
        ?>
        <tr class="produit">
          <td class="deletecol"><form class="" action="control/panier/remove.php" method="post">
            <input type="text" name="delete" value='<?php echo $i ?>' hidden>
            <button type="submit" name="button"><i class="fa fa-trash-o"></i></button>
          </form></td>
          <td><?php echo $produit['nom'] ?></td>
          <td class="hidden-xs"><?php echo $produit['description'] ?></td>
          <td><?php echo $_SESSION['panier']['qte'][$i] ?></td>
          <td><?php echo number_format($produit['prix'],2).' $'?></td>
          <td><?php echo $_SESSION['panier']['qte'][$i]*$produit['prix'].' $'?></td>
        </tr>
        <?php
      }
      //fermeture de la connexion
      $req->closeCursor();
      $connection=null;
      ?>
      <tr class="finalprice">
        <td class="right" colspan="5">Sous-Total : </td>
        <td><?php echo number_format($total,2)."$"?></td>
      </tr>
      <tr class="finalprice">
        <td class="right" colspan="5">Taxes : </td>
        <td><?php echo number_format($total*0.15,2)."$"?></td>
      </tr>
      <tr class="finalprice">
        <td class="right" colspan="5">Total : </td>
        <td ><?php echo number_format($total*1.15,2)."$"?></td>
      </tr>
      <?php
    }else {
      ?>
      <tr>
        <td><em>Votre panier est vide.</em></td>
      </tr>

      <?php
    }
    ?></table>
    <a class="erasepanier" href="control/panier/reset.php">Effacer le panier</a>
    <form class="" action="control/panier/achatFinal.php" method="post">
      <label for="typePaiment">Type de paiement : </label>
      <SELECT name="typePaiment" size="1">
        <option value="master">MasterCard</option>
        <option value="visa">Visa</option></SELECT>
	  <label for="noCarte">Numéro de carte : </label>
	  <input type="text" id="" value="" name="noCarte">
      <button type="submit" name="button">Envoyer la commande</button>
      </form></div>
