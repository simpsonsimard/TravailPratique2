<?php
/*
* Titre : afficherproduits.php
* Description : permet d'afficher tous les produits de la base de données
*/
//connexion BD
  require('control/param_bd.inc');
  $req = $connection->prepare('SELECT * FROM produits');
  $req->execute();
    while($produit = $req->fetch()){
?>
    <div class="card">
      <a href="<?php
      echo 'produit.php?item='.$produit['no']; ?>"><div class="cardPicture">

      <img src="<?php echo $produit['image']; ?>" alt=""></div>
      <div class="description">
        <h3><?php echo $produit['nom']; ?></h3>
      <p><?php echo $produit['description']; ?></p></a>
      </div>
    </div>
<?php
  }
  $req->closeCursor();
  $connection = null;
?>
