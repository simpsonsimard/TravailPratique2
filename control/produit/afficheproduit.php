<?php
/*
* Titre : afficheproduit.php
* Description : permet d'afficher un seul produit selon le GET
* voir : /produits.php
* note :
*/
//on regarde si item est dans la barre d'adresse
if (isset($_GET['item'])) {
  //connexion BD
  require('control/param_bd.inc');
  $req = $connection->prepare('SELECT * FROM produits WHERE no=:no');
  $req->execute(array('no'=>$_GET['item']));
  $produit = $req->fetch();
  if (!empty($produit)) {?>
    <div class="container oneProduct">
      <?php
      if (isset($_GET['code']))
      {
        if ($_GET['code']==1) {
          ?>
          <div class="alert error">
            <p>
          <?php
          echo "L'article que vous désirez n'est plus disponible pour la quantité demandée.";
        }
        if ($_GET['code']==0) {
          ?>
          <div class="alert succes">
            <p>
          <?php
          echo "L'article a été ajouté à votre panier.";
        }
      ?></p>
      </div><?php
      }
    ?>



        <div class="col-xs-12 col-md-6 imageContaint">
          <img src="<?php echo $produit['image'] ?>" alt=""></img>
        </div>
        <div class="col-xs-12 col-md-6 descContaint">
          <h2><?php echo $produit['nom'] ?></h2>
          <p><?php echo $produit['description'] ?></p>
          <div class="prix"><p>Prix unitaire : <?php echo $produit['prix'].'$' ?></p></div>
          <div class="quantiter"><p>Quantité disponible :  <?php echo $produit['qte'] ?></p></div>
          <p class="date"><p>Date de parution : <?php echo $produit['dateParution'] ?></p>
          <form class="" action="control/panier/ajout.php" method="post">
            <input type="text" name="produit" value="<?php echo $produit['no'] ?>" hidden>
            <SELECT name="qte" size="1">
              <?php
              for ($i=1; $i <= 10; $i++) {
                ?><option value="<?php echo $i ?>"><?php echo $i ?></option> <?php
              }?>
            </SELECT>
            <button type="submit" id="button">Ajouter au panier</button>
          </form>
        </div>
      </div>
    <?php
  }else {
    header('location: produits.php');
    exit;
  }
  $req->closeCursor();
  $connection = null;
}else{
  header('location: produits.php');
  exit;
}?>
