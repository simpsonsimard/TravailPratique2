<?php
$current_url = explode('?',  $_SERVER['HTTP_REFERER']);
if (isset($_POST['user']) && !empty($_POST['pass'])) {
  require('../param_bd.inc');
    $req = $connection->prepare('SELECT * FROM clients WHERE login = :user');
    $req->execute(array('user'=>$_POST['user']));
    if ($req->rowCount()==0) {
      $req->closeCursor();
      $req = $connection->prepare('INSERT INTO clients(nom, prenom, adresse, ville, province, codePostal, login, motPasse, email) VALUES (:nom,:prenom,:adresse,:ville,:province,:codePostal,:user,:motPasse,:email)');
      $error=false;
      foreach ($_POST AS $key => $value) {
        if (empty($value)) {
          $error=true;
        }
      }
      if ($error==false) {
        $req->execute(array(':nom'=>$_POST['name'],
        ':prenom'=>$_POST['prenom'],
        ':adresse'=>$_POST['adresse'],
        ':ville'=>$_POST['ville'],
        ':province'=>$_POST['province'],
        ':codePostal'=>$_POST['codePostal'],
        ':user'=>$_POST['user'],
        ':motPasse'=>$_POST['pass'],
        ':email'=>$_POST['email']));
        header('Location: ' . $current_url[0]."?code=0");
        exit();
      }else {
        header('Location: ' . $current_url[0]."?code=1");
        exit();
      }
    }else {
    header('Location: ' . $current_url[0]."?code=2");
    exit();
    }

}
header('Location: ' . $current_url[0]."?code=3");
exit();
 ?>
