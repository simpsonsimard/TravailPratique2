<?php
/*
* Titre : edit.php
* Description : modification d'un profil d'utilisateur (profil.php)
* voir /profil.php pour le post
*/
//connexion BD
require('../param_bd.inc');
session_start();
if (isset($_SESSION['utilisateur']['no'])) {
  $req = $connection->prepare('UPDATE clients SET
    nom=:nom,
    prenom=:prenom,
    adresse=:adresse,
    ville=:ville,
    province=:province,
    codePostal=:codePostal,
    email=:email
    WHERE no = :no');
  $req->execute(array(
    'nom'=> $_POST['nom'],
    'prenom'=> $_POST['prenom'],
    'adresse'=> $_POST['adresse'],
    'ville'=> $_POST['ville'],
    'province'=> $_POST['province'],
    'codePostal'=> $_POST['codePostal'],
    'email'=> $_POST['email'],
    'no'=> $_SESSION['utilisateur']['no']
  ));
  $req->closeCursor();
  $conn = null;
}
header('Location: ' . $_SERVER['HTTP_REFERER']);
exit();
 ?>
