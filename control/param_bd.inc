<?php

$dbHote="localhost";
$dbUtilisateur="root";
$dbMotPasse="";
$dbNom="tp2_evo_ms";

try {
   $connection = new PDO("mysql:host=localhost;dbname=$dbNom",$dbUtilisateur,$dbMotPasse,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
} catch (PDOException $e) {
  exit( "Erreur lors de la connexion à la BD ".$e->getMessage());
}
